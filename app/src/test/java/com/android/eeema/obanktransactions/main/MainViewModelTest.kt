package com.android.eeema.obanktransactions.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.android.eeema.obanktransactions.domain.mappers.ApiDateToViewDateMapper
import com.android.eeema.obanktransactions.domain.mappers.TransactionToMainViewDataMapper
import com.android.eeema.obanktransactions.domain.usecase.RetrieveTransactionsUseCase
import com.android.eeema.obanktransactions.fake.EmptyRepository
import com.android.eeema.obanktransactions.fake.ErrorRepository
import com.android.eeema.obanktransactions.fake.FakeRepository
import com.android.eeema.obanktransactions.main.model.MainItemType
import com.android.eeema.obanktransactions.rules.MainCoroutineScopeRule
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Currency
import java.util.Locale

class MainViewModelTest {

    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val esLocale = Locale("es", "ES")
    private val esDecimalFormat =
        (DecimalFormat.getCurrencyInstance(esLocale) as DecimalFormat).apply {
            applyPattern("#.##¤")
            minimumFractionDigits = 2
            minimumIntegerDigits = 1
            currency = Currency.getInstance(esLocale)
        }

    private val manager = mockk<StringManager>()
    private val today = mockk<Calendar>(relaxed = true)
    private val dateMapper = ApiDateToViewDateMapper(today, esLocale, manager)
    private val mapper = TransactionToMainViewDataMapper(dateMapper, manager, esDecimalFormat)
    private lateinit var useCase: RetrieveTransactionsUseCase
    private lateinit var sut: MainViewModel

    @Before
    fun setUp() {
        every { today.get(Calendar.DAY_OF_MONTH) } returns 21
        every { today.get(Calendar.MONTH) } returns 6
        every { today.get(Calendar.YEAR) } returns 2020
        every { manager.getString(R.string.today, any()) } returns "Today"
        every { manager.getString(R.string.yesterday, any()) } returns "Yesterday"
        every { manager.getString(R.string.no_description) } returns "No description"
    }

    @Test
    fun `given empty repository then retrieved list should be empty`() {
        useCase = RetrieveTransactionsUseCase(EmptyRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        assertThat(sut.transactions.value).isEmpty()
    }

    @Test
    fun `given error repository then retrieved list should be empty`() {
        useCase = RetrieveTransactionsUseCase(ErrorRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        assertThat(sut.transactions.value).isEmpty()
    }

    @Test
    fun `given fake repository then retrieved list should not be empty`() {
        useCase = RetrieveTransactionsUseCase(FakeRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        coroutineScope.advanceTimeBy(1_000)
        assertThat(sut.transactions.value).isNotEmpty()
    }

    @Test
    fun `given fake repository then retrieved then check first response`() {
        useCase = RetrieveTransactionsUseCase(FakeRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        coroutineScope.advanceTimeBy(1_000)

        val firstEntry = sut.transactions.value?.first()
        assertThat(firstEntry).isNotNull()
        assertThat(firstEntry!!.id).isEqualTo(1)
        assertThat(firstEntry.title).isEqualTo("Description 1")
        assertThat(firstEntry.date).isEqualTo("dom, 19 jul 2020")
        assertThat(firstEntry.value).isEqualTo("-22,40€")
        assertThat(firstEntry.type).isEqualTo(MainItemType.EXPENSE)
    }

    @Test
    fun `given fake repository then retrieved then check last response`() {
        useCase = RetrieveTransactionsUseCase(FakeRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        assertThat(sut.transactions.value).isNull()

        coroutineScope.advanceTimeBy(1_000)

        val firstEntry = sut.transactions.value?.last()
        assertThat(firstEntry).isNotNull()
        assertThat(firstEntry!!.id).isEqualTo(3)
        assertThat(firstEntry.title).isEqualTo("Description 3")
        assertThat(firstEntry.date).isEqualTo("Today")
        assertThat(firstEntry.value).isEqualTo("15,00€")
        assertThat(firstEntry.type).isEqualTo(MainItemType.ENTRY)
    }

    @Test
    fun `given type for empty list should return entry`() {
        useCase = RetrieveTransactionsUseCase(EmptyRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()

        assertThat(sut.getType(0)).isEqualTo(MainItemType.ENTRY)
    }

    @Test
    fun `given type for second item list should entry`() {
        useCase = RetrieveTransactionsUseCase(FakeRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()
        coroutineScope.advanceTimeBy(1_000)
        assertThat(sut.getType(1)).isEqualTo(MainItemType.ENTRY)
    }

    @Test
    fun `given type for first item list should expense`() {
        useCase = RetrieveTransactionsUseCase(FakeRepository, mapper)
        sut = MainViewModel(useCase)

        sut.retrieveData()
        coroutineScope.advanceTimeBy(1_000)
        assertThat(sut.getType(0)).isEqualTo(MainItemType.EXPENSE)
    }

    // TODO: Add test to check count and get functions from ViewModel
}

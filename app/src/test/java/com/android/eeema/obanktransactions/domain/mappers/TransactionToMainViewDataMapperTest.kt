package com.android.eeema.obanktransactions.domain.mappers

import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.android.eeema.obanktransactions.domain.model.Transaction
import com.android.eeema.obanktransactions.main.model.MainItemType
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Currency
import java.util.Locale

class TransactionToMainViewDataMapperTest {
    private val manager = mockk<StringManager>()
    private val today = mockk<Calendar>(relaxed = true)
    private val esLocale = Locale("es", "ES")

    private val usDecimalFormat =
        (DecimalFormat.getCurrencyInstance(Locale.US) as DecimalFormat).apply {
            applyPattern("¤#.##")
            minimumFractionDigits = 2
            minimumIntegerDigits = 1
            currency = Currency.getInstance(Locale.US)
        }

    private val esDecimalFormat =
        (DecimalFormat.getCurrencyInstance(esLocale) as DecimalFormat).apply {
            applyPattern("#.##¤")
            minimumFractionDigits = 2
            minimumIntegerDigits = 1
            currency = Currency.getInstance(esLocale)
        }

    private val dateMapper = ApiDateToViewDateMapper(today, esLocale, manager)

    private val sut = TransactionToMainViewDataMapper(dateMapper, manager, esDecimalFormat)

    @Before
    fun setUp() {
        every { today.get(Calendar.DAY_OF_MONTH) } returns 21
        every { today.get(Calendar.MONTH) } returns 6
        every { today.get(Calendar.YEAR) } returns 2020
        every { manager.getString(R.string.today, any()) } returns "Today"
        every { manager.getString(R.string.yesterday, any()) } returns "Yesterday"
        every { manager.getString(R.string.no_description) } returns "No description"
    }

    @Test
    fun `given transaction with null amount null should be returned`() {
        val transaction = Transaction(1, null, null, null, null)

        val result = sut.transform(transaction)

        assertThat(result).isNull()
    }

    @Test
    fun `given transaction with null date null should be returned`() {
        val transaction = Transaction(1, null, 20f, null, null)

        val result = sut.transform(transaction)

        assertThat(result).isNull()
    }

    @Test
    fun `given transaction with negative amount expense type should be return`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -20f, null, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.type).isEqualTo(MainItemType.EXPENSE)
    }

    @Test
    fun `given transaction with positive amount entry type should be return`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, 20f, null, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.type).isEqualTo(MainItemType.ENTRY)
    }

    @Test
    fun `given null fee then value should be the same as positive amount`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, 20f, null, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("20,00€")
    }

    @Test
    fun `given null fee then value should be the same as negative amount`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -21.15f, null, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("-21,15€")
    }

    @Test
    fun `given usLocale then decimal should be formatted as usd`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -21.15f, null, null)
        val usSut = TransactionToMainViewDataMapper(dateMapper, manager, usDecimalFormat)
        val result = usSut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("-$21.15")
    }

    @Test
    fun `given negative fee then should be sbustracted if amount is positive`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, 21f, -2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("19,00€")
    }

    @Test
    fun `given positive fee then should be added if amount is positive`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, 21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("23,00€")
    }

    @Test
    fun `given negative fee then should be added if amount is negative`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, -2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("-23,00€")
    }

    @Test
    fun `given positive fee then should be substracted if amount is negative`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.value).isEqualTo("-19,00€")
    }

    @Test
    fun `given invalid date format then null should be returned`() {
        val transaction = Transaction(1, "2020-13-07", 20f, null, null)

        val result = sut.transform(transaction)

        assertThat(result).isNull()
    }

    @Test
    fun `given empty date format then null should be returned`() {
        val transaction = Transaction(1, "", 20f, null, null)

        val result = sut.transform(transaction)

        assertThat(result).isNull()
    }

    @Test
    fun `given blank date format then null should be returned`() {
        val transaction = Transaction(1, "  ", 20f, null, null)

        val result = sut.transform(transaction)

        assertThat(result).isNull()
    }

    @Test
    fun `given today date then today should be shown`() {
        val date = "2020-07-21T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.date).isEqualTo("Today")
    }

    @Test
    fun `given yesterday date then yesterday should be shown`() {
        val date = "2020-07-20T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.date).isEqualTo("Yesterday")
    }

    @Test
    fun `given 2020-07-19T10_34_00_000Z date should return dom 19 jul 2020`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.date).isEqualTo("dom, 19 jul 2020")
    }

    @Test
    fun `given null description then no description should be shown`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, null)
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.title).isEqualTo("No description")
    }

    @Test
    fun `given blank description then no description should be shown`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, " ")
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.title).isEqualTo("No description")
    }

    @Test
    fun `given empty description then no description should be shown`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, "")
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.title).isEqualTo("No description")
    }

    @Test
    fun `given description then same description value should be shown`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(1, date, -21f, 2f, "Description")
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.title).isEqualTo("Description")
    }

    @Test
    fun `given id equals ten then id should be ten`() {
        val date = "2020-07-19T10:34:00.000Z"
        val transaction = Transaction(10, date, -21f, 2f, " ")
        val result = sut.transform(transaction)

        assertThat(result).isNotNull()
        assertThat(result!!.id).isEqualTo(10)
    }
}

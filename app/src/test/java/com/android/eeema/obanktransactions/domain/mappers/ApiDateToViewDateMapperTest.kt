package com.android.eeema.obanktransactions.domain.mappers

import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import java.util.Calendar
import java.util.Locale

class ApiDateToViewDateMapperTest {

    private val manager = mockk<StringManager>()
    private val today = mockk<Calendar>(relaxed = true)
    private val esLocale = Locale("es", "ES")
    private val enLocale = Locale.ENGLISH

    private val sut = ApiDateToViewDateMapper(today, esLocale, manager)
    private val enSut = ApiDateToViewDateMapper(today, enLocale, manager)

    @Before
    fun setUp() {
        every { today.get(Calendar.DAY_OF_MONTH) } returns 21
        every { today.get(Calendar.MONTH) } returns 6
        every { today.get(Calendar.YEAR) } returns 2020
        every { manager.getString(R.string.today, any()) } returns "Today"
        every { manager.getString(R.string.yesterday, any()) } returns "Yesterday"
    }

    @Test
    fun `null date should return null`() {
        val result = sut.transform(null)

        assertThat(result).isNull()
    }

    @Test
    fun `empty date should return null`() {
        val result = sut.transform("")

        assertThat(result).isNull()
    }

    @Test
    fun `invalid date string should return null`() {
        val result = sut.transform("hello")

        assertThat(result).isNull()
    }

    @Test
    fun `invalid date format should return null`() {
        val result = sut.transform("00:01 02-07-2017")

        assertThat(result).isNull()
    }

    @Test
    fun `date well formatted should not be null`() {
        val result = sut.transform("2020-07-21T10:34:00.000Z")

        assertThat(result).isNotNull()
    }

    @Test
    fun `given today date should return today text`() {
        val result = sut.transform("2020-07-21T10:34:00.000Z")

        assertThat(result).isEqualTo("Today")
    }

    @Test
    fun `given yesterday date should return yesterday text`() {
        val result = sut.transform("2020-07-20T10:34:00.000Z")

        assertThat(result).isEqualTo("Yesterday")
    }

    @Test
    fun `given 2020-07-19T10_34_00_000Z date should return dom 19 jul 2020`() {
        val result = sut.transform("2020-07-19T10:34:00.000Z")

        assertThat(result).isEqualTo("dom, 19 jul 2020")
    }

    @Test
    fun `given english locale2020-07-19T10_34_00_000Z date should return dom 19 jul 2020`() {
        val result = enSut.transform("2020-07-19T10:34:00.000Z")

        assertThat(result).isEqualTo("Sun, 19 Jul 2020")
    }
}

package com.android.eeema.obanktransactions.fake

import com.android.eeema.obanktransactions.datasources.Repository
import com.android.eeema.obanktransactions.domain.model.Transaction
import kotlinx.coroutines.delay
import java.lang.IllegalArgumentException

object FakeRepository : Repository {
    override suspend fun retrieveData(): List<Transaction> {
        delay(1_000)

        return listOf(
            Transaction(1, "2020-07-19T10:34:00.000Z", -21f, -1.4f, "Description 1"),
            Transaction(2, "2020-07-20T10:34:00.000Z", 10f, 2f, "Description 2"),
            Transaction(3, "2020-07-21T10:34:00.000Z", 19f, -4f, "Description 3")
        )
    }
}

object EmptyRepository : Repository {
    override suspend fun retrieveData(): List<Transaction> = emptyList()
}

object ErrorRepository : Repository {
    override suspend fun retrieveData(): List<Transaction> = throw IllegalArgumentException()
}

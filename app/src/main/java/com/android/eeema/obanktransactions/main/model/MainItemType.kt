package com.android.eeema.obanktransactions.main.model

enum class MainItemType {
    EXPENSE, ENTRY
}

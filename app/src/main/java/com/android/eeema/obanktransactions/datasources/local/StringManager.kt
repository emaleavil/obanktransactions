package com.android.eeema.obanktransactions.datasources.local

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class StringManager @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun getString(resourceId: Int, value: String = ""): String = if (value.isBlank()) {
        context.getString(resourceId)
    } else {
        context.getString(resourceId, value)
    }
}

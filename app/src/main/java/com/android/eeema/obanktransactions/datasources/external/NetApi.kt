package com.android.eeema.obanktransactions.datasources.external

import com.android.eeema.obanktransactions.domain.model.Transaction
import retrofit2.http.GET

interface NetApi {

    @GET("/transactions.json")
    suspend fun transactions(): List<Transaction>
}

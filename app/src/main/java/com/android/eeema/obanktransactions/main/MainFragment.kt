package com.android.eeema.obanktransactions.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.android.eeema.obanktransactions.databinding.FragmentMainBinding
import com.android.eeema.obanktransactions.utils.gone
import com.android.eeema.obanktransactions.utils.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main_empty_view.*
import kotlinx.android.synthetic.main.fragment_main_loading_view.*
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {

    @Inject
    lateinit var viewModel: MainViewModel

    @Inject
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        binding.transactionList.adapter = adapter
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        retrieveData()
        registerObservers()
    }

    private fun retrieveData() {
        viewModel.retrieveData()
        showLoading()
    }

    private fun registerObservers() {
        viewModel.transactions.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                adapter.notifyDataSetChanged()
                showList()
            } else {
                showEmptyView()
            }
        }
    }

    private fun showList(): Unit = with(binding) {
        noDataGroup.gone()
        loadingGroup.gone()
        transactionList.visible()
    }
    private fun showLoading(): Unit = with(binding) {
        noDataGroup.gone()
        transactionList.gone()
        loadingGroup.visible()
    }
    private fun showEmptyView(): Unit = with(binding) {
        loadingGroup.gone()
        transactionList.gone()
        noDataGroup.visible()
        retry.setOnClickListener {
            retrieveData()
        }
    }
}

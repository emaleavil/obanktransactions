package com.android.eeema.obanktransactions.main.adapter

import androidx.viewbinding.ViewBinding
import com.android.eeema.obanktransactions.databinding.ItemTransactionEntryBinding
import com.android.eeema.obanktransactions.main.model.MainViewData

class EntryRender(
    binding: ViewBinding
) : Render {

    private val viewBinding: ItemTransactionEntryBinding by lazy {
        binding as ItemTransactionEntryBinding
    }

    override fun render(data: MainViewData?): Unit = with(viewBinding) {
        data?.let {
            date.text = it.date
            title.text = it.title
            value.text = it.value
        }
    }
}

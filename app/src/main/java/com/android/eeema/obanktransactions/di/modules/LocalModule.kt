package com.android.eeema.obanktransactions.di.modules

import android.content.Context
import com.android.eeema.obanktransactions.datasources.local.StringManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object LocalModule {

    @Singleton
    @Provides
    fun provideStringManager(@ApplicationContext context: Context): StringManager {
        return StringManager(context)
    }
}

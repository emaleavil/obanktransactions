package com.android.eeema.obanktransactions.main.model

data class MainViewData(
    val id: Int,
    val title: String,
    val value: String,
    val date: String,
    val type: MainItemType
)

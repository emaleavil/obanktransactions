package com.android.eeema.obanktransactions.di.modules

import androidx.recyclerview.widget.RecyclerView
import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.Repository
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.android.eeema.obanktransactions.di.annotations.DateMapperQualifier
import com.android.eeema.obanktransactions.di.annotations.DefaultDecimalFormatQualifier
import com.android.eeema.obanktransactions.di.annotations.NetRepositoryQualifier
import com.android.eeema.obanktransactions.di.annotations.TransactionToMainViewDataQualifier
import com.android.eeema.obanktransactions.domain.mappers.ApiDateToViewDateMapper
import com.android.eeema.obanktransactions.domain.mappers.Mapper
import com.android.eeema.obanktransactions.domain.mappers.TransactionToMainViewDataMapper
import com.android.eeema.obanktransactions.domain.model.Transaction
import com.android.eeema.obanktransactions.domain.usecase.RetrieveTransactionsUseCase
import com.android.eeema.obanktransactions.main.MainViewModel
import com.android.eeema.obanktransactions.main.adapter.TransactionsAdapter
import com.android.eeema.obanktransactions.main.model.MainViewData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Currency
import java.util.Locale

@Module
@InstallIn(FragmentComponent::class)
object MainModule {

    @Provides
    @FragmentScoped
    fun provideViewModel(useCase: RetrieveTransactionsUseCase): MainViewModel {
        return MainViewModel(useCase)
    }

    @Provides
    @FragmentScoped
    fun provideUseCase(
        @NetRepositoryQualifier repository: Repository,
        @TransactionToMainViewDataQualifier mapper: Mapper<*, *>
    ): RetrieveTransactionsUseCase {
        return RetrieveTransactionsUseCase(repository, mapper as Mapper<Transaction, MainViewData?>)
    }

    @Provides
    @FragmentScoped
    fun provideAdapter(
        adapter: TransactionsAdapter
    ): RecyclerView.Adapter<RecyclerView.ViewHolder> {
        return adapter
    }

    @TransactionToMainViewDataQualifier
    @Provides
    @FragmentScoped
    fun provideMapper(
        @DateMapperQualifier dateMapper: Mapper<*, *>,
        manager: StringManager,
        @DefaultDecimalFormatQualifier decimalFormat: DecimalFormat
    ): Mapper<*, *> {
        return TransactionToMainViewDataMapper(dateMapper as Mapper<String?, String?>, manager, decimalFormat)
    }

    @DateMapperQualifier
    @Provides
    @FragmentScoped
    fun provideDateMapper(
        manager: StringManager
    ): Mapper<*, *> {
        return ApiDateToViewDateMapper(Calendar.getInstance(), Locale.getDefault(), manager)
    }

    @DefaultDecimalFormatQualifier
    @Provides
    @FragmentScoped
    fun provideDecimalFormatter(
        manager: StringManager
    ): DecimalFormat {
        return (DecimalFormat.getCurrencyInstance(Locale.getDefault()) as DecimalFormat).apply {
            applyPattern(manager.getString(R.string.decimal_pattern))
            minimumFractionDigits = 2
            minimumIntegerDigits = 1
            currency = Currency.getInstance(Locale.getDefault())
        }
    }
}

package com.android.eeema.obanktransactions.domain.usecase

abstract class BaseUseCase<in Params, out Output> {
    abstract suspend fun execute(params: Params): Output
}

object Empty : Any()

package com.android.eeema.obanktransactions

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OBankTransactionsApplication : Application()

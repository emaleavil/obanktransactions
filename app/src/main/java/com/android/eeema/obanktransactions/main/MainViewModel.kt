package com.android.eeema.obanktransactions.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.eeema.obanktransactions.domain.usecase.Empty
import com.android.eeema.obanktransactions.domain.usecase.RetrieveTransactionsUseCase
import com.android.eeema.obanktransactions.main.model.MainItemType
import com.android.eeema.obanktransactions.main.model.MainViewData
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val useCase: RetrieveTransactionsUseCase
) : ViewModel() {

    /*
        TODO: Instead using this we can use STATE and handle with Either
         to have error in one hand and STATE in the other
    */
    private val _transactions = MutableLiveData<List<MainViewData>>()
    val transactions: LiveData<List<MainViewData>>
        get() = _transactions

    fun retrieveData() {
        viewModelScope.launch {
            // TODO: should be handle errors like no network connection
            _transactions.value = try {
                useCase.execute(Empty)
            } catch (e: Throwable) {
                emptyList()
            }
        }
    }

    fun getType(position: Int): MainItemType {
        return try {
            transactions.value?.get(position)?.type ?: MainItemType.ENTRY
        } catch (e: Throwable) {
            MainItemType.ENTRY
        }
    }

    fun count(): Int {
        return transactions.value?.count() ?: 0
    }

    fun get(position: Int): MainViewData? {
        return try {
            _transactions.value?.get(position)
        } catch (e: Throwable) {
            null
        }
    }
}

package com.android.eeema.obanktransactions.utils

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

fun String?.toDate(
    format: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
    locale: Locale = Locale.getDefault()
): Calendar? {
    return this.runCatching {
        val now = Calendar.getInstance()
        val date = SimpleDateFormat(format, locale).parse(this)
        now.time = date
        now
    }.getOrNull()
}

fun String?.toTime(
    format: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
    locale: Locale = Locale.getDefault()
): Long? {
    return this.runCatching {
        val now = Calendar.getInstance()
        val date = SimpleDateFormat(format, locale).parse(this)
        date.time
    }.getOrNull()
}

fun Calendar.isToday(today: Calendar = Calendar.getInstance()): Boolean {
    return today.get(Calendar.DAY_OF_MONTH) == this.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.MONTH) == this.get(Calendar.MONTH) &&
        today.get(Calendar.YEAR) == this.get(Calendar.YEAR)
}

fun Calendar.isYesterday(today: Calendar = Calendar.getInstance()): Boolean {
    return today.get(Calendar.DAY_OF_MONTH) - 1 == this.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.MONTH) == this.get(Calendar.MONTH) &&
        today.get(Calendar.YEAR) == this.get(Calendar.YEAR)
}

fun Calendar.format(
    format: String = "EEE, dd MMM yyyy",
    locale: Locale = Locale.getDefault()
): String? {
    return this.runCatching {
        SimpleDateFormat(format, locale).format(this.time)
    }.getOrNull()
}

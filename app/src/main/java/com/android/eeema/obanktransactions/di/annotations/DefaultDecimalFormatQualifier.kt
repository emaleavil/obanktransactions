package com.android.eeema.obanktransactions.di.annotations

import javax.inject.Qualifier

@Qualifier
annotation class DefaultDecimalFormatQualifier

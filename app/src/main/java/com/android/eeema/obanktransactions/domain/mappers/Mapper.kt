package com.android.eeema.obanktransactions.domain.mappers

interface Mapper<in I, out O> {
    fun transform(input: I): O
}

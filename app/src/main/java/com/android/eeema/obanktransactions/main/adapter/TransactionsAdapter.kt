package com.android.eeema.obanktransactions.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.android.eeema.obanktransactions.databinding.ItemTransactionEntryBinding
import com.android.eeema.obanktransactions.databinding.ItemTransactionExpenseBinding
import com.android.eeema.obanktransactions.main.model.MainItemType
import com.android.eeema.obanktransactions.main.MainViewModel
import javax.inject.Inject

class TransactionsAdapter @Inject constructor(
    private val viewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = when (viewType) {
            MainItemType.ENTRY.ordinal -> ItemTransactionEntryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            MainItemType.EXPENSE.ordinal -> ItemTransactionExpenseBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            else -> ItemTransactionEntryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        }

        return Holder(binding, viewType)
    }

    override fun getItemCount(): Int {
        return viewModel.count()
    }

    override fun getItemViewType(position: Int): Int {
        return viewModel.getType(position).ordinal
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is Holder) {
            holder.render(position)
        }
    }

    inner class Holder(
        private val binding: ViewBinding,
        private val viewType: Int
    ) : RecyclerView.ViewHolder(binding.root) {

        fun render(position: Int) {
            when (viewType) {
                MainItemType.ENTRY.ordinal -> EntryRender(binding)
                MainItemType.EXPENSE.ordinal -> ExpensesRender(binding)
                else -> EntryRender(binding)
            }.render(viewModel.get(position))
        }
    }
}

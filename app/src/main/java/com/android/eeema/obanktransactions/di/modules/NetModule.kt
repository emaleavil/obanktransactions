package com.android.eeema.obanktransactions.di.modules

import com.android.eeema.obanktransactions.datasources.Repository
import com.android.eeema.obanktransactions.datasources.external.NetApi
import com.android.eeema.obanktransactions.datasources.external.NetRepository
import com.android.eeema.obanktransactions.di.annotations.NetRepositoryQualifier
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ApplicationComponent::class)
object NetModule {

    @Provides
    fun provideNetApi(): NetApi {
        val client = OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
            .build()

        return Retrofit.Builder()
            .baseUrl("https://code-challenge-e9f47.web.app")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(NetApi::class.java)
    }

    @NetRepositoryQualifier
    @Provides
    fun provideNetRepository(
        repository: NetRepository
    ): Repository {
        return repository
    }
}

package com.android.eeema.obanktransactions.domain.usecase

import com.android.eeema.obanktransactions.datasources.Repository
import com.android.eeema.obanktransactions.domain.mappers.Mapper
import com.android.eeema.obanktransactions.domain.model.Transaction
import com.android.eeema.obanktransactions.main.model.MainViewData
import javax.inject.Inject

class RetrieveTransactionsUseCase @Inject constructor(
    private val repository: Repository,
    private val mapper: Mapper<Transaction, MainViewData?>
) : BaseUseCase<Empty, List<MainViewData>>() {

    override suspend fun execute(params: Empty): List<MainViewData> {
        return repository.retrieveData().mapNotNull { transaction ->
            mapper.transform(transaction)
        }
    }
}

package com.android.eeema.obanktransactions.datasources

import com.android.eeema.obanktransactions.domain.model.Transaction

interface Repository {
    suspend fun retrieveData(): List<Transaction>
}

package com.android.eeema.obanktransactions.domain.mappers

import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.android.eeema.obanktransactions.domain.model.Transaction
import com.android.eeema.obanktransactions.main.model.MainItemType
import com.android.eeema.obanktransactions.main.model.MainViewData
import java.text.DecimalFormat
import javax.inject.Inject

class TransactionToMainViewDataMapper @Inject constructor(
    private val dateMapper: Mapper<String?, String?>,
    private val stringManager: StringManager,
    private val decimalFormat: DecimalFormat
) : Mapper<Transaction, MainViewData?> {

    override fun transform(input: Transaction): MainViewData? {
        if (input.amount == null || input.date == null) {
            return null
        }

        val type = if (input.amount < 0) {
            MainItemType.EXPENSE
        } else {
            MainItemType.ENTRY
        }

        val fee = input.fee ?: 0f
        val date = dateMapper.transform(input.date) ?: return null
        val value = decimalFormat.format(input.amount + fee)
        val description = input.description?.takeIf { it.isNotBlank() }
            ?: stringManager.getString(R.string.no_description)

        return MainViewData(input.id, description, value, date, type)
    }
}

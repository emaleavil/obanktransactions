package com.android.eeema.obanktransactions.main.adapter

import androidx.viewbinding.ViewBinding
import com.android.eeema.obanktransactions.databinding.ItemTransactionExpenseBinding
import com.android.eeema.obanktransactions.main.model.MainViewData

class ExpensesRender(
    binding: ViewBinding
) : Render {

    private val viewBinding: ItemTransactionExpenseBinding by lazy {
        binding as ItemTransactionExpenseBinding
    }

    override fun render(data: MainViewData?): Unit = with(viewBinding) {
        data?.let {
            date.text = it.date
            title.text = it.title
            value.text = it.value
        }
    }
}

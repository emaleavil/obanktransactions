package com.android.eeema.obanktransactions.main.adapter

import com.android.eeema.obanktransactions.main.model.MainViewData

interface Render {
    fun render(data: MainViewData?)
}

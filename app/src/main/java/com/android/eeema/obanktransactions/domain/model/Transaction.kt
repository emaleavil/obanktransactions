package com.android.eeema.obanktransactions.domain.model

import com.google.gson.annotations.SerializedName

data class Transaction(
    @SerializedName("id") val id: Int,
    @SerializedName("date") val date: String?,
    @SerializedName("amount") val amount: Float?,
    @SerializedName("fee") val fee: Float?,
    @SerializedName("description") val description: String?
)

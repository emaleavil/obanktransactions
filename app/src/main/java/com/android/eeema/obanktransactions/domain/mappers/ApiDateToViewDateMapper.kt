package com.android.eeema.obanktransactions.domain.mappers

import com.android.eeema.obanktransactions.R
import com.android.eeema.obanktransactions.datasources.local.StringManager
import com.android.eeema.obanktransactions.utils.format
import com.android.eeema.obanktransactions.utils.isToday
import com.android.eeema.obanktransactions.utils.isYesterday
import com.android.eeema.obanktransactions.utils.toDate
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject

class ApiDateToViewDateMapper @Inject constructor(
    private val now: Calendar,
    private val locale: Locale,
    private val stringManager: StringManager
) : Mapper<String?, String?> {

    override fun transform(input: String?): String? {
        return input?.transformToDate()
    }

    private fun String?.transformToDate(): String? {
        val date = this.toDate(locale = locale) ?: return null
        return when {
            date.isToday(now) -> stringManager.getString(R.string.today)
            date.isYesterday(now) -> stringManager.getString(R.string.yesterday)
            else -> date.format(locale = locale)
        }
    }
}

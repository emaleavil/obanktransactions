package com.android.eeema.obanktransactions.datasources.external

import com.android.eeema.obanktransactions.datasources.Repository
import com.android.eeema.obanktransactions.domain.model.Transaction
import com.android.eeema.obanktransactions.utils.toTime
import javax.inject.Inject

class NetRepository @Inject constructor(
    private val api: NetApi
) : Repository {

    override suspend fun retrieveData(): List<Transaction> {
        return api.transactions().sortedBy { it.date.toTime() }.asReversed()
    }
}

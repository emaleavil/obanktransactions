# Notas de desarrollo y decisiones de la prueba

- Aunque el proyecto es un proyecto simple he querido dimensionarlo teniendo en mente que éste fuese un proyecto de mayor dimensión.
- El desarrollo no ha sido realizado siguiente la metodología TDD, sino que los tests han sido posteriores al desarrollo.
- La aplicación utiliza Architecture components de Android para implementar la arquitectura MVVM, aunque también hace uso de capas de la arquitectura Clean (Algunas de las capas no aportan demasiado e incluso de podria prescindir de ellas ya que no aportan valor en una aplicación tan pequeña)
- Con respecto a la ordenación de la lista creo que no es la forma acertada de hacerlo, lo he hecho así porque es la forma más rapido y no quiero retrasarme más con la prueba, pero sí sé que es la que menos flexibilidad aporta y que probablemente conlleve realizar modificaciones a posteriori si se quisiese añadir distintos tipos de ordenación. Probablemente lo mejor sería mover la ordenación a los casos de uso y que desde la aplicación se suministrase el tipo de ordenación deseado. El problema es que al haber creado un modelo específico para la vista y traer la fecha con el formato que quiero para su visualización he generado el problema de que la ordenación sea más compleja.

### Acerca de los test unitarios

Aunque en los tests unitarios podría haber sido más exhaustivo, cosa que probablemente se podría haber conseguido realizando TDD en vez de realizar los tests a posteriori. Sí que he querido hacer tests que abarquen lo máximo posible. En primer lugar he creado dos tests unitarios para los Mappers de datos cuyo scope es más reducido y posteriormente he intentado dar un scope mayor al tests del viewmodel en dónde he teniendo en cuenta el comportamiento de algunas dependencias reales de éste tales como son los mapeadores de fecha y transacciones y el repositorio, para el cual me he creado tres fakes con el fin no tener dependencia de un agente externo. Con la rule creada específicamente para los tests he conseguido controlar el reloj simulando así una petición asíncrona, de esta manera podría controlar timeouts y testarlos de una manera sencilla.

### En la prueba se han utilizado las siguientes dependencias

* __Retrofit__: Se ha utilizado como librería para simplificar la llamada al servicio, se podría haber hecho utilizando OkHttp y haciéndolo manualmente pero debido al tiempo limitado de la prueba he tomado la decisión de hacerlo con esta librería.
* __Hilt__: Aunque este inyector de dependencias se encuentra todavía en alfa, y teniendo en cuenta que no lo utilizaría en un entorno productivo, he tomado la decisión de escogerlo para la prueba porque requiere menos tiempo de configuración que Dagger y la forma de utilizarlo es similar al estar construido a partir de éste.
* __Gson__: Se ha utilizado para la conversión del json de respuesta en objetos de la aplicación. Éste se usa aprovechando el conversor que ofrece retrofit.
* __Corutinas__: Se han utilizado para hacer los cambios de hilos dentro de la aplicación y así evitar realizar las llamadas de red en el hilo principal. Para ello se han aprovechado las facilidades de retrofit y de los architecture components de Android.
* __Navigation__: Aunque en este proyecto no sería necesario, debido a su simplicidad, he querido incluirlo con el fin de mostrar el uso de los componentes de Jetpack.
* __Truth__: Para asserts en los tests, también podría haber usado Hamcrest.
* __Mockk__: Para crear mocks en los tests, comentar que es la primera vez que lo utilizo, normalmente he trabajado con Mockito.